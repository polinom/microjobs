from django.conf.urls import patterns, include, url
from django.views.generic.simple import direct_to_template

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from tastypie.api import Api
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from microjob import settings
from applications.examinations.api import TestItemResource, TestUnitResource, AssessmentsResource
from applications.customers.api import UserGroupResource, UserResource, MembershipResource

v1_api = Api(api_name='v1')
v1_api.register(TestItemResource())
v1_api.register(TestUnitResource())
v1_api.register(AssessmentsResource())
v1_api.register(UserGroupResource())
v1_api.register(UserResource())
v1_api.register(MembershipResource())


urlpatterns = patterns('',

    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

    # url(r'^$', 'microjob.views.home', name='home'),
    # url(r'^microjob/', include('microjob.foo.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r"^account/", include("account.urls")),
    url(r"^$", direct_to_template, {"template": "homepage.html"}, name="home"),
    url(r"^app/", direct_to_template, {"template": "app.html"}, name="app"),
    url(r"^debug/", direct_to_template, {"template": "debug.html"}, name="app"),
    url(r'^api/', include(v1_api.urls)),
)

urlpatterns += staticfiles_urlpatterns()
