from django.utils.cache import add_never_cache_headers


class DisableClientSideCaching(object):
    def process_response(self, request, response):
        if request.path.startswith('/static/'):
            response['Cache-Control'] = 'must-revalidate, no-cache'
        return response