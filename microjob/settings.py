# Django settings for microjob project.

import sys
import os

def rel(*x):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)

sys.path.insert(0, rel('..', 'lib'))

DEBUG = not os.environ.get('IS_HEROKU_ENVIRONMENT', False)
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'db',                       # Or path to database file if using sqlite3.
        'USER': '',                       # Not used with sqlite3.
        'PASSWORD': '',                   # Not used with sqlite3.
        'HOST': '',                       # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                       # Set to empty string for default. Not used with sqlite3.
    }
}


TIME_ZONE = 'America/Chicago'

LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_NAME = 'MicroTrud'

EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

FIXTURE_DIRS = (
    rel('fixtures'),
)

MEDIA_ROOT = rel('..', 'files', 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = rel('..', 'files', 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    rel('..', 'applications', 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

SECRET_KEY = 's_kpyj63@+%l)mp=ji)&amp;b#r_91i9$3nrb@--@mzvx!+52kscv*'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #"account.middleware.LocaleMiddleware",
    #"account.middleware.TimezoneMiddleware",
    )


if DEBUG:
    MIDDLEWARE_CLASSES + ('microjob.utils.middlewares.DisableClientSideCaching',)


TEMPLATE_CONTEXT_PROCESSORS = [
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    "account.context_processors.account",
]

ROOT_URLCONF = 'microjob.urls'

WSGI_APPLICATION = 'microjob.wsgi.application'

TEMPLATE_DIRS = (
    rel('..', 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',

    #libs
    'gunicorn',
    'storages',
    'sorl.thumbnail',
    'django_extensions',
    'staging',
    'django_forms_bootstrap',
    'eventlog',
    'taggit',
    'tastypie',
    'django_gravatar',

    #appliations
    'applications.customers',
    'applications.examinations',
    'account',
    'microjob',

)


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# Profile
AUTH_PROFILE_MODULE = "customers.UserProfile"

# configures memcached on heroku and simulates memcached locally
from memcacheify import memcacheify
CACHES = memcacheify()


try:
    from settings_local import *
except ImportError:
    pass

if not DEBUG:
    from heroku_settings import *
    # get postgress backend from heroku gloal config
    import dj_database_url
    DATABASES['default'] = dj_database_url.config()


# this code have to be always last in settings file
# runs declarated items of different modules that support declaration
import microjob.startup as startup
startup.run()


