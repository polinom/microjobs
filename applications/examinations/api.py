from tastypie.resources import ModelResource
from models import Assessments, TestUnit, TestItem
from tastypie.authorization import Authorization
from tastypie.authentication import SessionAuthentication
import json
from django.conf.urls.defaults import *
from django.core.paginator import Paginator, InvalidPage
from django.http import Http404
from tastypie.utils import trailing_slash


class BaseModelResource(ModelResource):

    def determine_format(self, request):
        return 'application/json'


class TestItemResource(BaseModelResource):
    LIST_OF_BODY_KEYS = ['title', 'question', 'answers', 'points', 'active', 'need_check']

    class Meta:
        authorization = Authorization()
        authentication = SessionAuthentication()
        list_allowed_methods = ['get', 'post', 'put', 'delete']
        queryset = TestItem.objects.filter()
        always_return_data = True

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(user=request.user)

    def save(self, *args, **kwargs):
        bundle = super(TestItemResource, self).save(*args, **kwargs)
        if bundle.data['tags'][0]:
            bundle.obj.tags.set(*bundle.data['tags'])
        else:
            bundle.obj.tags.clear()
        return bundle

    def dehydrate(self, bundle):
        body_data = eval(bundle.data['body'])
        del bundle.data['body']
        bundle.data = dict(body_data.items() + bundle.data.items())
        bundle.data['tags'] = list(bundle.obj.tags.all())
        return bundle

    def hydrate(self, bundle):
        bundle.data['body'] = {}
        for key, value in bundle.data.items():
            if key in self.LIST_OF_BODY_KEYS:
                bundle.data['body'][key] = value
        bundle.data['body'] = json.dumps(bundle.data['body'])
        return bundle

    # search
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/search%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_search'), name="api_get_search"),
        ]

    def get_search(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        search_string = request.GET.get('q', '')

        # Do the query.
        sqs = self._meta.queryset.filter(body__icontains=search_string)
        paginator = Paginator(sqs, 20)

        try:
            page = paginator.page(int(request.GET.get('page', 1)))
        except InvalidPage:
            raise Http404("Sorry, no results on that page.")

        objects = []

        for obj in page.object_list:
            bundle = self.build_bundle(obj=obj, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'meta': {},
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)



class TestUnitResource(BaseModelResource):

    class Meta:
        queryset = TestUnit.objects.filter()
        always_return_data = True

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(authors=request.user)

    def obj_create(self, bundle, request=None, **kwargs):
        return super(TestUnitResource, self).obj_create(bundle, autohors=request.user)

    def hydrate(self, bundle):
        print bundle
        return bundle


class AssessmentsResource(BaseModelResource):

    class Meta:
        queryset = Assessments.objects.filter()
        always_return_data = True

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(user=request.user)

    def obj_create(self, bundle, request=None, **kwargs):
        return super(AssessmentsResource, self).obj_create(bundle, user=request.user)

    def hydrate(self, bundle):
        print bundle
        return bundle
