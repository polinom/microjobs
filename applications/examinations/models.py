from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from microjob.utils.fields import JSONField
from taggit.managers import TaggableManager


class Assessments(models.Model):
    ASSASSMENT_TYPES = (
        (1, _('Questionnaire')),
        (2, _('Time framed Assasment')),
        (4, _('Always Available')),
       )

    STATUSES = (
        (1, _('Private')),
        (2, _('Public')),
        (3, _('Public Protected'))
        )

    user = models.ForeignKey(User)
    title = models.CharField(max_length=255)
    description = models.TextField(max_length=1024)
    status = models.PositiveIntegerField(choices=STATUSES)
    test_units = models.ManyToManyField('TestUnit')
    type = models.PositiveIntegerField(choices=ASSASSMENT_TYPES)
    time_start = models.DateTimeField(null=True)
    time_end = models.DateTimeField(null=True)
    time_given = models.PositiveIntegerField(null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)


class TestUnit(models.Model):
    TESTUNIT_STATUS = (
            (1, _('Personal')),
            (2, _('Free Public')),
            (2, _('For Sale')),
        )
    title = models.CharField(max_length=255)
    test_items = models.ManyToManyField('TestItem')
    autohors = models.ManyToManyField(User)
    status = models.PositiveIntegerField(choices=TESTUNIT_STATUS)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    max_score = models.PositiveIntegerField(default=0)

    tags = TaggableManager()


class TestItem(models.Model):
    TEST_TYPES = (
                  (1, _('One Answer')),
                  (2, _('Multiple Answers')),
                  (3, _('Without Choice One Answer')),
                  (4, _('Without Choice Multiple Answers'))
                )
    user = models.ForeignKey(User)
    body = JSONField(max_length=1024)
    test_type = models.PositiveIntegerField(choices=TEST_TYPES)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    tags = TaggableManager()


