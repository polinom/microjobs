from django.contrib import admin
from models import TestItem


class TestItemAdmin(admin.ModelAdmin):
    list_display = ["user", "body", "test_type", "active"]


admin.site.register(TestItem, TestItemAdmin)
