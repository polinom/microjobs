

require.config({
    baseUrl: window.settings.STATIC_URL+"js/",
    // 3rd party script alias names (Easier to type "jquery" than "libs/jquery, etc")
    // probably a good idea to keep version numbers in the file names for updates checking
    paths:{
        // Core Libraries
        "jquery": "libs/jquery",
        "jqueryui": "libs/jqueryui",
        "underscore":"libs/lodash",
        "backbone":"libs/backbone",
        "marionette":"libs/backbone.marionette",
        "handlebars":"libs/handlebars",

        // Plugins
        "backbone.validateAll":"libs/plugins/Backbone.validateAll",
        "bootstrap":"libs/plugins/bootstrap",
        "text":"libs/plugins/text",
        "backbone-forms": "libs/backbone-forms",
        "list": "libs/plugins/list",
        "jquery.cookie": "libs/plugins/jquery.cookie",
        'jquery.pnotify': "libs/plugins/jquery.pnotify",
        "underscore.string": 'libs/underscore.string',
        "bootstrap-modalmanager": "libs/plugins/bootstrap-modalmanager",
        "bootstrap-modal": "libs/plugins/bootstrap-modal",

        // Application Folders
        "collections":"app/collections",
        "models":"app/models",
        "routers":"app/routers",
        "templates":"app/templates",
        "config":"app/config",
        "controllers":"app/controllers",
        "views":"app/views",
        "parts":"app/parts",
        "commands": "app/commands"
    },
    // Sets the configuration for your third party scripts that are not AMD compatible
    shim:{
        // Twitter Bootstrap jQuery plugins
        "bootstrap":["jquery"],
        // jQueryUI
        "jqueryui":["jquery"],
        // Backbone
        "backbone":{
            // Depends on underscore/lodash and jQuery
            "deps":["underscore", "jquery"],
            // Exports the global window.Backbone object
            "exports":"Backbone"
        },
        //Marionette
        "marionette":{
            "deps":["underscore", "backbone", "jquery"],
            "exports":"Marionette"
        },
        //Handlebars
        "handlebars":{
            "exports":"Handlebars"
        },
        // Backbone.validateAll plugin that depends on Backbone
        "backbone.validateAll":["backbone"],
        "underscore.string": ["underscore"],
        "backbone-forms": ["underscore.string"],
        "list": ["bootstrap-modal", "bootstrap-modalmanager"]
    }
});

// Includes Desktop Specific JavaScript files here (or inside of your Desktop router)
require(["jquery", "backbone", "marionette", "app/App", "jqueryui", "bootstrap", "backbone.validateAll"],
    function ($) {
        // Start Marionette Application in desktop mode (default)
        App.start();
    });