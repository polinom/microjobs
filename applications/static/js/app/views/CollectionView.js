define(['underscore',
        'jquery',
        'handlebars',
        'collections/Collection',
        './ItemView'
        ],
    function (_, $, Handlebars, Collection, ItemView) {
        return Backbone.Marionette.CollectionView.extend({
            itemView: ItemView,
            collection: new Collection(),

            initialize: function(){

            },

            showCollection: function(){
                var ItemView;
                this.collection.each(function(item, index){
                  item.set('index', index+1);
                  ItemView = this.getItemView(item);
                  this.addItemView(item, ItemView, index);
                }, this);
            }
        });
    });