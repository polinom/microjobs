define(['underscore',
    'jquery',
    'handlebars',
    './HeaderMenuItemsCollection',
    './HeaderMenuItemView',
    'text!./Header.html'
],
    function (_, $, Handlebars, collection, item_view, template) {
        return Backbone.Marionette.CompositeView.extend({
            tagName: "div",
            className: "navbar",
            itemView: item_view,
            collection: collection,
            template: Handlebars.compile(template),
            itemViewContainer: ".nav",

            initialize: function(){
                this.on('itemview:state:active', this.changeItemStateActive);
            },

            changeItemStateActive: function(item){
                var url = item.model.get('id') === 'index'?'':item.model.get('id');
                App.appRouter.navigate(url, true);
                item.model.set('state', 'active');
                var active = this.collection.where({state : 'active'});
                var past = _.without(active, item.model)[0];
                past.set('state', null);
            }
        });
    });