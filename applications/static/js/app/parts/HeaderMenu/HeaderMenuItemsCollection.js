define(['backbone', 'marionette'], function(Backbone, Marionette) {
    return new Backbone.Collection(window.settings.header_menu);
});