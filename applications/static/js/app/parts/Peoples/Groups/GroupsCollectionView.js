define(['underscore',
        'jquery',
        'handlebars',
        './GroupCollection',
        './GroupItemView',
        'text!./group_view.html'
],
    function (_, $, Handlebars, collection, item_view, template) {
        return Backbone.Marionette.CompositeView.extend({
            itemView: item_view,
            collection: collection,
            itemViewContainer: ".groups",
            template: Handlebars.compile(template)
        });
    });