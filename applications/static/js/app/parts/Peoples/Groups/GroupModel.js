define(["jquery", "backbone", 'models/Model'],
    function($, Backbone, Model) {
        // Creates a new Backbone Model class object
        return Model.extend();
    }
);