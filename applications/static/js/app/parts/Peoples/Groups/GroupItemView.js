define( ['underscore', 'jquery', 'handlebars', 'text!./group_item.html'],
    function(_, $, Handlebars, template) {
        return Backbone.Marionette.ItemView.extend( {
            template: Handlebars.compile(template),
            tagName: 'li',

            initialize: function(){
                console.log("initialize");
            },

            events: {
                'click span': 'addToGroup'
            },

            addToGroup: function(e){
                console.log();
               App.appRouter.navigate('peoples/groups/view/'+this.model.get('id'), true);
            }
        });
    });