define(['backbone', 'marionette', './GroupModel', 'collections/Collection'], function(Backbone, Marionette, GroupModel, Collection) {
    var GroupCollection = Collection.extend({
        model: GroupModel,
        url: '/api/v1/usergroup/',

        initialize: function(options){
            this.fetch();
        }
    });
    return new GroupCollection();
});