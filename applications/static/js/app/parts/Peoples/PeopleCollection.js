define(['backbone', 'marionette', './PersonModel', 'collections/Collection'], function(Backbone, Marionette, PersonModel, Collection) {
    var PeopleCollection = Collection.extend({
        model: PersonModel,
        url: '/api/v1/user/',

        initialize: function(){
            this.fetch();
        }

    });
    return new PeopleCollection();
});