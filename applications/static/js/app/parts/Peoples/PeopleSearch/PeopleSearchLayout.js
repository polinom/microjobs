define(['underscore',
        'jquery',
        'handlebars',
        'text!./index.html',
        'routers/AppRouter',
        './SearchCollectionView',
        './SearchCollection'],
    function (_, $, Handlebars, template, AppRouter, SearchCollectionView, SearchCollection) {

        return Backbone.Marionette.Layout.extend( {
            template: Handlebars.compile(template),
            url_prefix: 'search/',
            collection_url: SearchCollection.url,

             regions: {
                searchResults: '#search-results'
             },

            ui: {
                input: "input[name='search-users']",
                animation: '.anim'
            },

            initialize: function(options){
                this.on('show', function(){
                    this.ui.animation.hide()
                });
            },

            events: {
                'keyup input[name="search-users"]': 'inputSearch'
            },

            inputSearch: function(event) {
                var query = this.searchValue();
                if (query && query.length){
                    this.ui.animation.show(); // show loading animation
                    App.modal.currentView.searchResults.show(
                        new SearchCollectionView({ collection: this.newCollection() })
                    );
                    this.ui.animation.hide(); // hide loading animation
                } else {
                    App.modal.currentView.searchResults.close();
                }
            },

            newCollection: function(){
                var collection = SearchCollection;
                collection.url = this.getUrl(collection);
                collection.fetch();
                return collection;
            },

            getUrl: function(collection){
                return this.collection_url + this.url_prefix + '?q=' + encodeURIComponent(this.searchValue());
            },

            searchValue: function(){
                return $.trim(this.ui.input.val());
            }
        });
    });