define(['backbone', 'marionette', '../PersonModel', 'collections/Collection'], function(Backbone, Marionette, PersonModel, Collection) {
    var SearchCollection = Collection.extend({
        model: PersonModel,
        url: '/api/v1/user/',

        initialize: function(options){
            this.fetch();
        }
    });
    return new SearchCollection();
});