define(['underscore',
        'jquery',
        'handlebars',
        './SearchItemView' ],
    function (_, $, Handlebars, item_view) {
        return Backbone.Marionette.CollectionView.extend({
            tagName: "div",
            className: "",
            itemView: item_view
        });
    });