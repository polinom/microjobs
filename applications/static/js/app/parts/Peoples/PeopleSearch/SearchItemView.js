define( ['underscore', 'jquery', 'handlebars', 'models/Model', 'text!./search_item.html'],
    function(_, $, Handlebars, Model, template) {
        return Backbone.Marionette.ItemView.extend( {
            template: Handlebars.compile(template),
            tagName: 'div',
            className: 'well well-small',

            initialize: function(){

            },

            // View Event Handlers
            events: {
                'click .add': 'addToGroup'
            },

            addToGroup: function(event){
                event.preventDefault();
                var collection = App.mainRegion.currentView.peoples.currentView.collection;
                var modelValue = this.model.toJSON();
                var existingModels = _.find(collection.models, function(element){
                    return JSON.stringify(element.toJSON()) === JSON.stringify(modelValue);
                });
                if (!existingModels){
                    collection.add(this.model);
                } else {
                    $.pnotify({text: "You've already added this user in the list!", type: "info"});
                }
            }
        });
    });