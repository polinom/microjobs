define( ['underscore', 'jquery', 'handlebars', 'models/Model', 'text!./person.html'],
    function(_, $, Handlebars, Model, template) {
        return Backbone.Marionette.ItemView.extend( {
            template: Handlebars.compile(template),
            tagName: 'div',
            className: 'span4',

            initialize: function(){

            },

           events: {
                'click #edit': 'editPerson',
                'click #remove': 'removePerson'
           },

            editPerson: function(event){
                console.log(this);
                //var view = new HeaderView(); // just for example
                //App.modal.show(view)
            },

            removePerson: function(event){
                event.preventDefault();
                App.mainRegion.currentView.peoples.currentView.collection.remove(this.model);
            }
        });
    });