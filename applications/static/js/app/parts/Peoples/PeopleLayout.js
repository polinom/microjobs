define( ['underscore', 'jquery', 'handlebars', 'models/Model', 'text!./people_layout.html'],
    function(_, $, Handlebars, Model, template) {

        return Backbone.Marionette.Layout.extend( {
            template: Handlebars.compile(template),

             regions: {
                peoples: '#people',
                toolbar: "#toolbar",
                groups: '#groups'
             }
        });
    });