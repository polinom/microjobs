define(['underscore',
        'jquery',
        'handlebars',
        'text!./toolbar.html',
        'routers/AppRouter',
        './PeopleSearch/PeopleSearchLayout'],
    function (_, $, Handlebars, template, AppRouter, PeopleSearchLayout) {
        return Backbone.Marionette.ItemView.extend({
            tagName: 'div',
            template: Handlebars.compile(template),

            events: {
                'click .add-users': 'addUsers',
                'click .new-group': 'createGroup'
            },

            addUsers: function(event){
                var view = new PeopleSearchLayout(); // just for example
                App.modal.show(view)
            },

            createGroup: function(event){
                var view = new HeaderView(); // just for example
                App.modal.show(view)
            }

        });
    });