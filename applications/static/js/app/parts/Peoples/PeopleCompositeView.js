define(['underscore',
    'jquery',
    'handlebars',
    './PeopleCollection',
    './PersonView',
    'text!./people_view.html'
],
    function (_, $, Handlebars, collection, item_view, template) {
        return Backbone.Marionette.CompositeView.extend({
            itemView: item_view,
            collection: collection,
            template: Handlebars.compile(template),

            initialize: function () {
                this.listenTo(this.collection, "add", this.modelAdded);
            },

            modelAdded: function (model, collection, options) {
                model.set("index", collection.length)
            },

            close: function () {
                this.stopListening();
            }
        });
    });