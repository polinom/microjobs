define(['underscore',
        'jquery',
        'handlebars',
        'backbone',
        'text!./search.html',
        'collections/Collection'],
    function (_, $, Handlebars, backbone, template, Collection) {
        return Backbone.Marionette.ItemView.extend({
            tagName: 'div',
            template: Handlebars.compile(template),
            searchButton: true,
            placeholder: 'text to search',
            collectionClass: Collection,
            url_prefix: 'search/',
            maxStringLengh: 2,

            events: {
                'focus input': 'focused',
                'keyup': 'keyPressed',
                'click button': 'submited'
            },

             getUrl: function(collection){
               return collection.url + this.url_prefix + '?q=' + encodeURIComponent(this.getValue()); 
             },

             search: function(){
                if (this.getValue().length >= this.maxStringLengh || !this.getValue()){
                    var collection = new this.collectionClass();
                    collection.url = this.getUrl(collection);
                    collection.fetch();
                    return collection;
                }
             },

             getValue: function(){
                  return this.$el.find('input').val();
             },

             keyPressed: function(event){
                 this.trigger('changed', this);
             },
             
             focused: function(event){
                 this.trigger('focus', this);
             },

             submited: function(){
                this.trigger('submit', this);
             },

             serializeData: function(){
                return {
                    button: this.searchButton,
                    placeholder: this.placeholder
                };

             }
        });
    });