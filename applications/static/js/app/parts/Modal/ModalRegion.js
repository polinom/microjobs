define(['backbone', 'marionette'], function (Backbone, Marionette) {
    return Backbone.Marionette.Region.extend({
        el: ".modal",

        onShow : function(view) {
			// 'hidden' - Bootstrap Modal event fired when the modal has finished being hidden from the user
			this.$el.on('hidden', this.close);
			view.on('close', this.hideModal, this);
			this.showModal(view);
		},

		showModal : function(view) {
			this.$el.modal('show');
		},

		hideModal : function() {
			this.$el.modal('hide');
		}
    });
});

