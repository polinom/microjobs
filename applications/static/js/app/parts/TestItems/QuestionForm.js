define(['underscore',
        'backbone',
        'jquery',
        'handlebars',
        'backbone-forms',
        'list',
        'libs/plugins/backbone.bootstrap-modal',
        'jquery.pnotify'],
    function (_, Backbone, $, Handlebars) {
        Backbone.Form.editors.List.Modal.ModalAdapter = Backbone.BootstrapModal;

        OneAnswerValidator = function(value, formValues){
          var correct_answers = _.where(formValues.answers, {correct: true});
          var false_answers = _.where(formValues.answers, {correct: false});
          var question_type = formValues.test_type;
          if (!correct_answers.length && (question_type == 1 || question_type == 3) || correct_answers.length > 1) {
                return {
                    type: 'answers',
                    message: 'This type of question requires one corect answer.'
                };}
          if (correct_answers.length <2 && (question_type == 2 || question_type == 4)) {
                return {
                    type: 'answers',
                    message: 'This type of question required multiple corect answers.'
                };}
          if (!false_answers.length && (question_type == 1 || question_type == 2)) {
                return {
                    type: 'answers',
                    message: 'Please add at least one false answer.'
                };}
        };
        return Backbone.Form.extend({

            save_button: true,

            //Schema
            schema: {
              test_type:  {
                        title: 'Test Type',
                        type: 'Select',
                        validators: ['required'],
                        options: [
                                {val:1, label: 'One Answer'},
                                {val:2, label: 'Multiple Answers'},
                                {val:3, label: 'Without Choice One Answer'},
                                {val:4, label: 'Without Choice Multiple Answers'}]
                         },
                question:   {type: 'TextArea', validators: ['required']},
                answers:     {
                               validators: [OneAnswerValidator],
                               type: 'List',
                               itemType: 'Object',
                               subSchema: {
                                       answer_variant: { type: 'Text', validators: ['required'] },
                                       correct: 'Checkbox',
                                       points: 'Number'
                               }
               },
                need_check: {title: 'Need Check', type: 'Checkbox', help: "Uncheck this option if you require to check this question by yourself"},
                tags:       'ComaSeparatedList'
            },

            initialize: function() {
              Backbone.Form.prototype.initialize.apply(this, arguments);
              this.on('test_type:change', function(form, typeEditor){
                console.log('type changed');
              });
            },

            onSaveBotton: function(e){
               e.preventDefault();
               var errors = this.commit();
               if (errors) return;
               //this.model.save()
               this.model.save({}, {'success':function(){
                    App.appRouter.navigate('questions', true);
                    $.pnotify({text: 'Successuly saved!', type: 'success'});
               }});
            },

            onDeleteBotton: function(e){
              this.model.destroy();
              App.appRouter.navigate('questions', true);
            }
        });
    });