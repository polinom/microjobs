define(['underscore',
        'jquery',
        'handlebars',
        'text!./toolbar.html',
        'routers/AppRouter',
        './QuestionSearch'],
    function (_, $, Handlebars, template, AppRouter, QuestionSearch) {
        return Backbone.Marionette.ItemView.extend({
            tagName: 'div',
            className: 'toolbar',
            template: Handlebars.compile(template),

            initialize: function(options){
                this.on('show', function(){
                   this.search = new QuestionSearch();
                   this.$el.prepend(this.search.render().el);
                });
             },

            events: {
                'click .new-question': 'newQuestion'
            },

            newQuestion: function(e){
                App.appRouter.navigate('questions/edit/', true);
            }

        });
    });