define( ['underscore', 'jquery', 'handlebars', 'models/Model', 'text!./question_layout.html'],
    function(_, $, Handlebars, Model, template) {
        QRegion = Backbone.Marionette.Region.extend({
            open: function(view){
                this.$el.find('table').append(view.el);
            }
        });

        //ItemView provides some default rendering logic
        return Backbone.Marionette.Layout.extend( {
            //Template HTML string
            template: Handlebars.compile(template),

             regions: {
                questions: {selector: "#questions", regionType: QRegion},
                toolbar: {selector: "#toolbar", regionType: Backbone.Marionette.Region}
               }
        });
    });