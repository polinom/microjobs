define(["jquery", "backbone", 'models/Model'],
    function($, Backbone, Model) {
        // Creates a new Backbone Model class object
        return Backbone.Model.extend({

            urlRoot: '/api/v1/testitem/'

            // url: function(){
            //     return this.urlRoot + this.isNew()?this.get('id'):'' + '/';
            // },

            // Default values for all of the Model attributes

        });
    }

);