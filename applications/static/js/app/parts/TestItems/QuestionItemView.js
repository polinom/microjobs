define(['underscore',
        'jquery',
        'handlebars',
        'text!./question.html',
        'routers/AppRouter',
        './QuestionForm'],
    function (_, $, Handlebars, template, AppRouter, QuestionForm) {
        return Backbone.Marionette.ItemView.extend({
            tagName: 'tr',
            template: Handlebars.compile(template),

            initialize: function(options){
             },

            events: {
                'click': 'editQuestion'
            },

            editQuestion: function(e){
                App.appRouter.navigate('questions/edit/'+this.model.get('id'), true);
            }

        });
    });