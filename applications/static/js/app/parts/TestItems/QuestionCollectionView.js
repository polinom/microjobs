define(['underscore',
        'jquery',
        'handlebars',
        './QuestionsCollection',
        './QuestionItemView'
        ],
    function (_, $, Handlebars, QuestionsCollection, item_view) {
        return Backbone.Marionette.CollectionView.extend({
            tagName: "tbody",
            className: "",
            itemView: item_view,
            collection:  new QuestionsCollection(),

            initialize: function(){
                this.collection.fetch();
            }
        });
    });