define(['underscore',
        'jquery',
        'handlebars',
        'backbone',
        'parts/search/SearchView',
        './QuestionsCollection',
        './QuestionCollectionView'],
    function (_, $, Handlebars, backbone, SearchView, QuestionsCollection, QuestionCollectionView) {
        return SearchView.extend({
            className: 'question-search',
            collectionClass: QuestionsCollection,
            maxStringLengh: 2,

            submited: function(){
                var q_view = new QuestionCollectionView({collection: this.search()});
                App.mainRegion.currentView.questions.show(q_view);
            },

            keyPressed: function(events){
                if (event.keyCode !== 32 && event.keyCode >= 65 && event.keyCode <= 90 || this.getValue() === '') {
                    this.submited();
               }
            }
        });
    });