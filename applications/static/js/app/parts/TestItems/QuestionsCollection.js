define(['backbone', 'marionette', './QuestionModel', 'collections/Collection'], function(Backbone, Marionette, QuestionModel, Collection) {
    return Collection.extend({
       model: QuestionModel,
       url: '/api/v1/testitem/',

      initialize: function(){
        }
    });
});