define(['backbone', 'marionette'], function(Backbone, Marionette) {
    return new Backbone.Collection(window.settings.main_menu);
});