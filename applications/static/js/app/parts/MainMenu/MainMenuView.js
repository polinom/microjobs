define(['underscore',
        'jquery',
        'handlebars',
        './MenuItemsCollection',
        './MainMenuItemView'
        ],
    function (_, $, Handlebars, collection, item_view) {
        return Backbone.Marionette.CollectionView.extend({
            tagName: "ul",
            className: "nav nav-list",
            itemView: item_view,
            collection: collection,

            initialize: function(){
                this.on('itemview:state:active', this.changeItemStateActive);
            },

            changeItemStateActive: function(item){
                var url = item.model.get('id') === 'index'?'':item.model.get('id');
                App.appRouter.navigate(url, true);
                this.changeMenuState(item.model);
            },

            changeMenuState: function(model){
                model.set('state', 'active');
                var active = this.collection.where({state : 'active'});
                var past = _.without(active, model)[0];
                past.set('state', null);
            },

            changeMenuStateByUrl: function(url){
              var exact_url = url === 'index'?'':url;
              current = this.collection.where({id : exact_url})[0];
              this.changeMenuState(current);
            }
        });
    });