define(['underscore',
        'jquery',
        'handlebars',
        'text!./MainMenuItem.html',
        'routers/AppRouter'],
    function (_, $, Handlebars, template, AppRouter) {
        return Backbone.Marionette.ItemView.extend({
            tagName: 'li',
            template: Handlebars.compile(template),
            events: {
                "click a" : "onClick"
               },

             initialize: function(options){
               this.model.on('change', this.render, this);
               this.model.on('change:state', this.chagedState, this);
             },

             onClick: function(e){
                e.preventDefault();
                this.model.set('state','active');
             },

             chagedState: function(){
                if (this.model.get('state') == 'active'){
                    this.trigger('state:active', this);
                 }
             },

             onRender: function(){
                var state = this.model.get('state');
                if (state){
                    this.$el.addClass('active');
                } else {
                    this.$el.removeClass('active');
                }
             }

        });
    });