define(['backbone', 'marionette'], function(Backbone, Marionette) {
   return Backbone.Marionette.AppRouter.extend({
       appRoutes: {
           // index
           ""        :           "index",
           // peoples
           "peoples" :           "peoples",
           "peoples/groups/view/:id": "viewGroup",
           // questions
           "questions":          "questions",
           "questions/edit/:id": "editQuestion",
           "questions/edit/":     "editQuestion"
       }
   });
});