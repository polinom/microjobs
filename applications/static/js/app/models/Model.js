define(["jquery", "backbone"],
    function ($, Backbone) {
        // Creates a new Backbone Model class object
        var Model = Backbone.Model.extend({

            base_url: function () {
                var temp_url = Backbone.Model.prototype.url.call(this);
                return (temp_url.charAt(temp_url.length - 1) == '/' ? temp_url : temp_url + '/');
            },

            url: function () {
                return this.base_url();
            }

        });

        // Returns the Model class
        return Model;

    }

);