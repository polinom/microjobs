define(['jquery', 'underscore', 'handlebars', 'routers/AppRouter', 'jquery.cookie', 'parts/Modal/ModalRegion'],
    function ($, _, Handlebars, AppRouter, notifications, ModalRegion) {
        //App is global variable for our instance of Backbone.Marionette.Application
        if (!window.App) {

            // redefine Backbone.sync for PUT and POST requests. Include csrf token to header
            Backbone.oldSync = Backbone.sync;
            Backbone.sync = Backbone.sync = function( method, model, options ) {
                options.error = function (xhr, ajaxOptions, thrownError) {
                    $.pnotify({text: 'Some error happened! \n We are working on it!', type: "error"});
                };
                var headers = _.extend( {}, options.headers );
                headers[ 'X-CSRFToken' ] = $.cookie( 'csrftoken' );
                options.headers = headers;
                return Backbone.oldSync( method, model, options );
            };

            var App = new Backbone.Marionette.Application();

            //Organize Application into regions corresponding to DOM elements
            //Regions can contain views, Layouts, or subregions nested as necessary
            App.addRegions({
                headerRegion:"header",
                mainMenuRegion: "#main_menu",
                mainRegion: "#main",
                modal: ModalRegion
            });

            App.addInitializer(function (options) {
                var dependencies;
                this.mobile = options ? options.mobile : false;
                if (this.mobile) {
                    dependencies = ["controllers/MobileController"];
                }
                else {
                    dependencies = ["controllers/DesktopController"];
                }
                //set AppRouter's controller to MobileController or DesktopController
                require(dependencies, function (Controller) {
                    App.appRouter = new AppRouter({
                        controller: new Controller()
                    });
                    Backbone.history.start();
                });
            });

        }
        //export App as global variable
        window.App = App;
    });