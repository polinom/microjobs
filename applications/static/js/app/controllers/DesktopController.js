define(['backbone',
        'marionette',
        'parts/ContentArea/InitialView',
        'parts/HeaderMenu/HeaderView',
        'parts/MainMenu/MainMenuView',
        'parts/Peoples/PeopleLayout',
        'parts/Peoples/PeopleCompositeView',
        'parts/Peoples/PeopleToolbarView',
        'parts/Peoples/Groups/GroupsCollectionView',
        'parts/TestItems/QuestionsLayout',
        'parts/TestItems/QuestionCollectionView',
        'parts/TestItems/QuestionsToolbarView',
        'parts/TestItems/QuestionForm',
        'parts/TestItems/QuestionModel'
        ], function (Backbone,
                     Marionette,
                     InitialView,
                     HeaderView,
                     MainMenuView,
                     PeopleLayout,
                     PeoplesCompositeView,
                     PeopleToolbarView,
                     GroupsCollectionView,
                     QuestionsLayout,
                     QuestionCollectionView,
                     QuestionsToolbarView,
                     QuestionForm,
                     QuestionModel) {
            return Backbone.Marionette.Controller.extend({
                initialize:function (options) {
                    App.headerRegion.show(new HeaderView());
                    App.mainMenuRegion.show(new MainMenuView());
                },

                //gets mapped to in AppRouter's appRoutes
                index:function () {
                    App.mainRegion.show(new InitialView());
                    App.mainMenuRegion.currentView.collection.get({'id':'index'}).set('state','active');
                },

                peoples: function(){
                    var peopleLayout = new PeopleLayout();
                    App.mainRegion.show(peopleLayout);
                    peopleLayout.peoples.show(new PeoplesCompositeView());
                    peopleLayout.toolbar.show(new PeopleToolbarView());
                    peopleLayout.groups.show(new GroupsCollectionView());
                    App.mainMenuRegion.currentView.collection.get({'id':'peoples'}).set('state','active');
                },

                questions: function(){
                    var question_layout = new QuestionsLayout();
                    App.mainRegion.show(question_layout);
                    question_layout.questions.show(new QuestionCollectionView());
                    question_layout.toolbar.show(new QuestionsToolbarView());
                    App.mainMenuRegion.currentView.collection.get({'id':'questions'}).set('state','active');
                },

                editQuestion: function(id){
                    // TODO: fetch media from serrver if currentView is not QuestionCollectionView
                    var model;
                    if (id){
                        model = new QuestionModel({'id': id});
                        model.fetch({'success': function(){
                            var form = new QuestionForm({model: model});
                            App.mainRegion.show(form);
                        }});
                    } else {
                        model = new QuestionModel();
                        var form = new QuestionForm({model: model});
                        App.mainRegion.show(form);
                    }
                },

                viewGroup: function(id){
                    console.log(id);
                }
                
            });
});