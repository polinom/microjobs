from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from models import UserProfile, UserGroup, Membership

admin.site.unregister(User)


class UserProfileInline(admin.StackedInline):
    model = UserProfile


class UserProfileAdmin(UserAdmin):
    inlines = [UserProfileInline]


class UserGroupAdmin(admin.ModelAdmin):
    pass


class MembershipAdmin(admin.ModelAdmin):
    pass

admin.site.register(UserGroup, UserGroupAdmin)
admin.site.register(User, UserProfileAdmin)
admin.site.register(Membership, MembershipAdmin)
