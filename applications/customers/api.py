from pprint import pprint
from tastypie.resources import ModelResource
from django.conf.urls import url
from models import UserGroup, User, Membership
from tastypie.authorization import Authorization
from tastypie.authentication import SessionAuthentication
from tastypie import fields
from django.core.paginator import Paginator, InvalidPage
from tastypie.utils import trailing_slash
from django.http import Http404


# class GroupResource(ModelResource):
#
#     class Meta:
#         authorization = Authorization()
#         authentication = SessionAuthentication()
#         queryset = Group.objects.all()
#         excludes = ['user', ]
#         allowed_methods = ['get', 'post', 'put', 'delete']
#
#     def obj_create(self, bundle, request=None, **kwargs):
#         return super(GroupResource, self).obj_create(bundle, request, user=request.user)


class UserResource(ModelResource):

    class Meta:
        authorization = Authorization()
        authentication = SessionAuthentication()
        queryset = User.objects.all()
        resource_name = 'user'
        excludes = ['email', 'password', 'is_active', 'is_staff', 'is_superuser', 'date_joined', 'last_login']
        allowed_methods = ['get', 'post', 'put', 'delete']

    # search
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/search%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_search'), name="api_get_search"),
            ]

    def get_search(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        search_string = request.GET.get('q', '')

        # Do the query.
        sqs = self._meta.queryset.filter(username__icontains=search_string)
        paginator = Paginator(sqs, 20)

        try:
            page = paginator.page(int(request.GET.get('page', 1)))
        except InvalidPage:
            raise Http404("Sorry, no results on that page.")

        objects = []

        for obj in page.object_list:
            bundle = self.build_bundle(obj=obj, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'meta': {},
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)


class MembershipResource(ModelResource):
    user = fields.ForeignKey(UserResource, "member", full=True)

    class Meta:
        authorization = Authorization()
        authentication = SessionAuthentication()
        queryset = Membership.objects.all()
        always_return_data = True
        allowed_methods = ['get', 'post', 'put', 'delete']

    def get_object_list(self, request):
        id = request.path.split("/")
        print id
        return super(MembershipResource, self).get_object_list(request)


class UserGroupResource(ModelResource):
    #owner = fields.ForeignKey(UserResource, 'owner', full=True)
    users = fields.ToManyField(MembershipResource, 'membership_set')

    class Meta:
        authorization = Authorization()
        authentication = SessionAuthentication()
        queryset = UserGroup.objects.all()
        always_return_data = True
        excludes = ['created', 'updated']
        allowed_methods = ['get', 'post', 'put', 'delete']

    def get_object_list(self, request):
        return super(UserGroupResource, self).get_object_list(request).filter(owner=request.user)

    def obj_create(self, bundle, request=None, **kwargs):
        # Finish this method to create preper objects on POST and PUT methods
        return super(UserGroupResource, self).obj_create(bundle, request)


