from django.db import models
from sorl.thumbnail import ImageField
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from cStringIO import StringIO
from countries import COUNTRY_CHOICES
from django.core.urlresolvers import reverse


def file_name(instance, filename):
    return '/'.join(['avatars', instance.user.username, filename])


class UserProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    GENDER_CHOICES = [
        ('M', 'Male'),
        ('F', 'Female'),
    ]
    gender = models.CharField(max_length='1', choices=GENDER_CHOICES, null=True, blank=True)
    birthday = models.DateField(null=True, blank=True)

    address = models.OneToOneField('Address', null=True, blank=True)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    home_num = models.CharField(max_length=20, null=True, blank=True)
    cell_num = models.CharField(max_length=20, null=True, blank=True)
    image = ImageField(upload_to=file_name, null=True, blank=True)
    description = models.TextField(max_length=1024, null=True, blank=True)
    homepage = models.CharField(max_length=254, null=True, blank=True)

    def _getDigitsOnly(self, value):
        if isinstance(value, basestring):
            s = StringIO()
            for c in value:
                if c.isdigit():
                    s.write(c)
            s.seek(0)
            return s.read()
        return None

    def set_address(self, country, state, zip, city, street, home_num='', cell_num='', confirmed=True):
        add_created = False
        created = False
        if home_num != '' or cell_num != '':
            self.home_num = self._getDigitsOnly(home_num)
            self.cell_num = self._getDigitsOnly(cell_num)
            created = True
        addr = self.address
        if addr is None:
            addr = Address()
            add_created = True
            created = True
        addr.country = country
        addr.state = state
        addr.zip = zip
        addr.city = city
        addr.street = street
        addr.confirmed = confirmed
        addr.save()
        if add_created:
            self.address = addr
        if created:
            self.save()

    def __unicode__(self):
        return self.user.username


class Address(models.Model):
    country = models.CharField(max_length=2, choices=COUNTRY_CHOICES)
    state = models.CharField(max_length=50)
    zip = models.CharField(max_length=10)
    city = models.CharField(max_length=50)
    street = models.CharField(max_length=200)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        c = self.city if self.city is not None else ' '
        return c + ' ' + self.country

    def get_absolute_url(self):
        return reverse('account_address')


class Organisation(models.Model):
    user = models.ManyToManyField(User, through="Type")
    title = models.CharField(max_length=100)
    address = models.OneToOneField(Address, null=True, blank=True)
    logo = models.ImageField(upload_to="media/logos/%Y/%m/", null=True, blank=True)
    description = models.CharField(max_length=1024, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)

    def __unicode__(self):
        return self.title


class Type(models.Model):
    ORGANISATION_TO_USER_TYPES = (
            (1, 'Administrator'),
            (2, 'Editor'),
        )

    user = models.ForeignKey(User)
    organisation = models.ForeignKey(Organisation)
    relation_type = models.PositiveIntegerField(choices=ORGANISATION_TO_USER_TYPES)


class UserGroup(models.Model):
    title = models.CharField(max_length=255)
    owner = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return 'Group "%s" owned by %s' % (self.title, self.owner.username)


class Membership(models.Model):
    group = models.ForeignKey(UserGroup)
    # this is student user
    member = models.ForeignKey(User, blank=True, null=True)

    def __unicode__(self):
        return 'User "%s" in group %s' % (self.member, self.group.title)

    class Meta:
        unique_together = (
            ('member', 'group')
        )


def user_created_callback(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(user_created_callback, User)
